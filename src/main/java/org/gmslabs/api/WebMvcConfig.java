package org.gmslabs.api;

import org.apache.catalina.Context;
import org.apache.tomcat.util.scan.StandardJarScanFilter;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
        sessionLocaleResolver.setDefaultLocale(Locale.FRANCE);
        return sessionLocaleResolver;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    /**
     * Bean permettant de supprimer
     * WARN  o.a.t.u.s.StandardJarScanner.log - Failed to scan [...] from classloader hierarchy
     *
     * @return
     */
    @Bean
    public TomcatServletWebServerFactory tomcatFactory() {
        return new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                Set<String> pattern = new LinkedHashSet<>();
                pattern.add("jaxb*.jar");
                pattern.add("jaxws*.jar");
                StandardJarScanFilter filter = new StandardJarScanFilter();
                filter.setTldSkip(org.springframework.util.StringUtils.collectionToCommaDelimitedString(pattern));
                ((StandardJarScanner) context.getJarScanner()).setJarScanFilter(filter);
            }
        };
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // Cahnge interceptor
        registry.addInterceptor(localeChangeInterceptor());
    }
}

