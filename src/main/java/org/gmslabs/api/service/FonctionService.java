package org.gmslabs.api.service;

import org.gmslabs.api.model.Fonction;

public interface FonctionService {

    /**
     * obtenirFonctionByCode
     *
     * @param code String
     *
     */
    Fonction obtenirFonctionByCode(String code);
}
