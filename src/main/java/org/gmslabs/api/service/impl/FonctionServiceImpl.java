package org.gmslabs.api.service.impl;

import org.gmslabs.api.model.Fonction;
import org.gmslabs.api.repository.FonctionRepository;
import org.gmslabs.api.service.FonctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FonctionServiceImpl implements FonctionService {
    @Autowired
    private FonctionRepository fonctionRepository;

    @Override
    public Fonction obtenirFonctionByCode(String code){
        return fonctionRepository.findByCode(code);
    }
}
