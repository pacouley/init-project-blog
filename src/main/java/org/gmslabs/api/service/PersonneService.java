package org.gmslabs.api.service;

import org.gmslabs.api.model.Personne;

public interface PersonneService {

    /**
     * obtenirByEmail
     *
     * @param email String
     *
     */
    Personne obtenirPersonneByEmail(String email);
}
