package org.gmslabs.api.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@RestController
public class BaseRestController {

    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock r = rwl.readLock();
    private final Lock w = rwl.writeLock();

    protected final void verrouillageLecture(){
        r.lock();
    }

    protected final void verrouillageEcriture(){
        w.lock();
    }
}
