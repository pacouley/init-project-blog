package org.gmslabs.api.web;

import org.gmslabs.api.dto.personne.PersonneDTO;
import org.gmslabs.api.dto.personne.PersonneMapper;
import org.gmslabs.api.service.PersonneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonneRestController extends BaseRestController {
    @Autowired
    private PersonneService personneService;

    @GetMapping("/personnes/{email}")
    public PersonneDTO obtenirPersonneByEmail(@PathVariable String email) {
        return PersonneMapper.INSTANCE.personneToDTO(personneService.obtenirPersonneByEmail(email));
    }
}
