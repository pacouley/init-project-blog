package org.gmslabs.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * Objet asssocié au informations de la personne
 *
 * @author pacouley
 */
@Entity
@Table(name = "personne_relation")
@Data
@NoArgsConstructor
public class PersonneRelation extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "relationfk")
    private Personne relation;

    private String lien;
}