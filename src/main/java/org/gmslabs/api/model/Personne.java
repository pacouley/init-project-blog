package org.gmslabs.api.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.*;

/**
 * Objet asssocié au informations de la personne
 *
 * @author pacouley
 */
@Entity
@Table(name = "personne")
@Data
@NoArgsConstructor
public class Personne extends BaseEntity {

    private String email;

    private String nom;

    private String prenom;

    @OneToMany(mappedBy = "relation")
    private Set<PersonneRelation> relations = new HashSet<PersonneRelation>();

    @OneToMany
    @JoinTable(
            name = "personne_fonction",
            joinColumns = @JoinColumn(name = "personnefk"),
            inverseJoinColumns = @JoinColumn(name = "fonctionfk")
    )
    private List<Fonction> fonctions;
}