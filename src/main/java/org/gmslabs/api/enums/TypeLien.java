package org.gmslabs.api.enums;

/**
 * Les roles dans l'application openrlg
 *
 * @author pacouley
 */
public enum TypeLien {

    // ****************************************************************
    // ATTRIBUTS
    // ****************************************************************
    /**
     * HUSBAND (Marie).
     */
    HUSBAND,

    /**
     * WIFE (femme).
     */
    WIFE,

    /**
     * cohabitant (concubin)
     */
    COHABITANT,

    /**
     * CHILD (enfant)
     */
    CHILD,

    /**
     * MANAGER (responsable)
     */
    MANAGER,

    /**
     * EMPLOYE (employé)
     */
    EMPLOYE;

    // ****************************************************************
    // CONSTRUCTEURS
    // ****************************************************************
    /**
     * Constructeur.
     */
    TypeLien() {
    }

    // ****************************************************************
    // METHODES
    // ****************************************************************

    // Start of user code other_method

    // End of user code

    // ****************************************************************
    // ACCESSEURS
    // ****************************************************************

}
