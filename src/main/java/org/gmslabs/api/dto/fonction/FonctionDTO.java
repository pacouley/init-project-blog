package org.gmslabs.api.dto.fonction;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * FonctionDTO
 *
 * @author pacouley
 */
@Data
@NoArgsConstructor
public class FonctionDTO {

    /**
     * code.
     */
    private String code;

    /**
     * libelle.
     */
    private String libelle;
}
