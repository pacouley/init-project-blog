package org.gmslabs.api.dto.fonction;

import org.gmslabs.api.model.Fonction;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper(componentModel = "spring")
public interface FonctionMapper {
    FonctionMapper INSTANCE = Mappers.getMapper( FonctionMapper.class );
    FonctionDTO fonctionToDTO(Fonction fonction);
    Fonction dtoToFonction(FonctionDTO fonctionDTO);
    List<FonctionDTO> fonctionToListDTO(List<Fonction> listFonction);
}
