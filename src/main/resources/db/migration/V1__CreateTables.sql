CREATE TABLE personne(
  id bigint IDENTITY,
  code_user_to_create varchar(50),
  code_user_to_update varchar(50),
  date_crea_enr timestamp,
  date_modif_enr timestamp,
  email varchar(50),
  nom varchar(50),
  prenom varchar(50)
);

CREATE TABLE fonction(
  id bigint IDENTITY,
  code_user_to_create varchar(50),
  code_user_to_update varchar(50),
  date_crea_enr timestamp,
  date_modif_enr timestamp,
  code varchar(50),
  libelle varchar(50)
);

CREATE TABLE personne_fonction(
    personnefk bigint NOT NULL,
    fonctionfk bigint NOT NULL,
    FOREIGN KEY(personnefk) REFERENCES personne(id),
    FOREIGN KEY(fonctionfk) REFERENCES fonction(id)
);

CREATE TABLE personne_relation(
    personnefk bigint NOT NULL,
    relationfk bigint NOT NULL,
    PRIMARY KEY (personnefk,relationfk),
    FOREIGN KEY (personnefk) REFERENCES personne (id),
    FOREIGN KEY (relationfk) REFERENCES personne (id)
);