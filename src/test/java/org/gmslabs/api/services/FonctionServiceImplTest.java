package org.gmslabs.api.services;

import org.gmslabs.api.model.Fonction;
import org.gmslabs.api.repository.FonctionRepository;
import org.gmslabs.api.service.FonctionService;
import org.gmslabs.api.service.impl.FonctionServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class FonctionServiceImplTest {

    @TestConfiguration
    static class FonctionServiceImplTestContextConfiguration {
        @Bean
        public FonctionService fonctionService() {
            return new FonctionServiceImpl();
        }
    }

    @Autowired
    private FonctionService fonctionService;

    @MockBean
    private FonctionRepository fonctionRepository;

    @Before
    public void setUp() {
        Fonction fonction = new Fonction();
        fonction.setCode("code");
        fonction.setLibelle("libelle");
        Mockito.when(fonctionRepository.findByCode(fonction.getCode())).thenReturn(fonction);
    }

    @Test
    public void testFindFonctionByLibelle() {
        String code = "code";
        Fonction found = fonctionService.obtenirFonctionByCode(code);
        assertThat(found.getCode()).isEqualTo(code);
        assertThat(found.getLibelle()).isEqualTo("libelle");
    }
}

