package org.gmslabs.api.repository;

import org.gmslabs.api.config.H2TestProfileJPAConfig;
import org.gmslabs.api.enums.TypeLien;
import org.gmslabs.api.model.Personne;
import org.gmslabs.api.model.PersonneRelation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { H2TestProfileJPAConfig.class },
        loader = AnnotationConfigContextLoader.class)
@Transactional
public class PersonneRepositoryTest {

    @Resource
    private PersonneRepository personneRepository;

    @Test
    public void testPersonne() {

        String email = "email";
        String libelleRole = "libelleRole";

        // Créer l'objet adresse
        Personne personne = new Personne();
        personne.setEmail(email);
        personneRepository.save(personne);

        // when
        Personne found = personneRepository.findByEmail(email);
        // then
        assertThat(found.getEmail()).isEqualTo(email);
    }

    @Test
    public void testParentPersonne(){
        String email1 = "email1";
        Personne papa = new Personne();
        papa.setEmail(email1);

        String email2 = "email2";
        Personne maman = new Personne();
        maman.setEmail(email2);

        // email3
        String email3 = "email3";
        Personne enfant1 = new Personne();
        enfant1.setEmail(email3);
        // email4
        String email4 = "email4";
        Personne enfant2 = new Personne();
        enfant2.setEmail(email4);

        // Mari
        PersonneRelation mari = new PersonneRelation();
        mari.setLien(TypeLien.HUSBAND.toString());
        mari.setRelation(papa);

        // fille
        PersonneRelation fille = new PersonneRelation();
        fille.setLien(TypeLien.CHILD.toString());
        fille.setRelation(enfant1);
        // fils
        PersonneRelation fils = new PersonneRelation();
        fils.setLien(TypeLien.CHILD.toString());
        fils.setRelation(enfant2);

        maman.setRelations(new HashSet<>(Arrays.asList(mari,fille,fils)));

        personneRepository.save(enfant1);
        personneRepository.save(enfant2);
        personneRepository.save(papa);
        personneRepository.save(maman);

        // when
        Personne foundMaman = personneRepository.findByEmail(email2);
        // then
        assertThat(foundMaman.getEmail()).isEqualTo(email2);
        // Relations
        Set<PersonneRelation> relations = foundMaman.getRelations();
        assertThat(relations).isNotNull();
    }
}
