package org.gmslabs.api.repository;

import org.gmslabs.api.config.H2TestProfileJPAConfig;
import org.gmslabs.api.model.Fonction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { H2TestProfileJPAConfig.class },
        loader = AnnotationConfigContextLoader.class)
@Transactional
public class FonctionRepositoryTest {

    @Resource
    private FonctionRepository fonctionRepository;

    @Test
    public void testFonction() {

        String code = "code";
        String libelle = "libelle";

        // Créer l'objet adresse
        Fonction fonction = new Fonction();
        fonction.setCode(code);
        fonction.setLibelle(libelle);
        fonctionRepository.save(fonction);

        // when
        Fonction found = fonctionRepository.findByCode(fonction.getCode());
        // then
        assertThat(found.getLibelle()).isEqualTo(libelle);
        // Role
        assertThat(found.getCode()).isEqualTo(code);
    }
}
