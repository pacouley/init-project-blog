package org.gmslabs.api.mapper;

import org.gmslabs.api.dto.personne.PersonneDTO;
import org.gmslabs.api.dto.personne.PersonneMapper;
import org.gmslabs.api.model.Personne;
import org.junit.Test;
import org.springframework.test.context.TestPropertySource;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@TestPropertySource(locations = "classpath:application-test.properties")
public class PersonneMapperTest {

    @Test
    public void mappingTestDePersonneVersPersonneDTO() {
        //given
        Personne personne = new Personne();
        personne.setEmail("email");

        //when
        PersonneDTO personneDTO = PersonneMapper.INSTANCE.personneToDTO(personne);

        //then
        assertThat(personne).isNotNull();
        assertThat(personneDTO.getEmail()).isEqualTo(personne.getEmail());
    }

    @Test
    public void mappingTestDePersonneDTOVersPersonne() {
        //given
        PersonneDTO personneDTO = new PersonneDTO();
        personneDTO.setEmail("email");

        //when
        Personne personne = PersonneMapper.INSTANCE.dtoToPersonne(personneDTO);

        //then
        assertThat(personneDTO).isNotNull();
        assertThat(personne.getEmail()).isEqualTo(personneDTO.getEmail());
    }

    @Test
    public void mappingTestDeListePersonneDTOVersListePersonne() {
        //given
        // personne1
        Personne personne1 = new Personne();
        personne1.setEmail("email1");
        // personne2
        Personne personne2 = new Personne();
        personne2.setEmail("email2");
        List<Personne> listePersos = Arrays.asList(personne1,personne2);

        //when
        List<PersonneDTO> listePersosDto = PersonneMapper.INSTANCE.personneToListDTO(listePersos);

        //then
        assertThat(listePersosDto).isNotNull();
        assertThat(listePersosDto.size()).isEqualTo(2);
    }
}