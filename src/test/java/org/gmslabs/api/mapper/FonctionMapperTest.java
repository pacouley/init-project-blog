package org.gmslabs.api.mapper;

import org.gmslabs.api.dto.fonction.FonctionDTO;
import org.gmslabs.api.dto.fonction.FonctionMapper;
import org.gmslabs.api.model.Fonction;
import org.junit.Test;
import org.springframework.test.context.TestPropertySource;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

@TestPropertySource(locations = "classpath:application-test.properties")
public class FonctionMapperTest {

    @Test
    public void mappingTestDeFonctionVersFonctionDTO() {
        //given
        Fonction fonction = new Fonction();
        fonction.setLibelle("libelle");
        fonction.setCode("code");

        //when
        FonctionDTO fonctionDTO = FonctionMapper.INSTANCE.fonctionToDTO(fonction);

        //then
        assertThat(fonctionDTO).isNotNull();
        assertThat(fonctionDTO.getLibelle()).isEqualTo(fonction.getLibelle());
        assertThat(fonctionDTO.getCode()).isEqualTo(fonction.getCode());
    }

    @Test
    public void mappingTestDeFonctionDTOVersFonction() {
        //given
        FonctionDTO fonctionDTO = new FonctionDTO();
        fonctionDTO.setLibelle("libelle");
        fonctionDTO.setCode("code");

        //when
        Fonction fonction = FonctionMapper.INSTANCE.dtoToFonction(fonctionDTO);

        //then
        assertThat(fonction).isNotNull();
        assertThat(fonction.getLibelle()).isEqualTo(fonctionDTO.getLibelle());
        assertThat(fonction.getCode()).isEqualTo(fonctionDTO.getCode());
    }

    @Test
    public void mappingTestDeListeFonctionDTOVersListeFonction() {
        //given
        // fonction1
        Fonction fonction1 = new Fonction();
        fonction1.setCode("code1");
        fonction1.setLibelle("libelle1");
        // fonction2
        Fonction fonction2 = new Fonction();
        fonction2.setCode("code2");
        fonction2.setLibelle("libelle2");
        List<Fonction> listeFonctions = Arrays.asList(fonction1,fonction2);

        //when
        List<FonctionDTO> listeFonctsDto = FonctionMapper.INSTANCE.fonctionToListDTO(listeFonctions);

        //then
        assertThat(listeFonctsDto).isNotNull();
        assertThat(listeFonctsDto.size()).isEqualTo(2);
    }
}