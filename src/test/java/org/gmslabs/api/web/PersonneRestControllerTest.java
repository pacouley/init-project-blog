package org.gmslabs.api.web;

import org.gmslabs.api.model.Personne;
import org.gmslabs.api.service.PersonneService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonneRestController.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class PersonneRestControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private PersonneService service;

    @Test
    @WithMockUser(value="user")
    @Ignore
    public void testObtenirOrganimseParCode()
            throws Exception {

        Personne personne = new Personne();
        personne.setEmail("email");

        given(service.obtenirPersonneByEmail("email")).willReturn(personne);

        mvc.perform(get("/api/personne/email").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.email", is("email")));
    }
}
