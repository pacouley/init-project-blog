//package org.openrlg.app.webmvc;
//
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Ignore;
//import org.junit.Test;
//import org.springframework.mock.web.MockHttpServletRequest;
//import org.springframework.mock.web.MockHttpServletResponse;
//import org.springframework.security.web.PortResolver;
//import org.springframework.security.web.PortResolverImpl;
//import org.springframework.security.web.savedrequest.DefaultSavedRequest;
//import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
//import org.springframework.security.web.savedrequest.SavedRequest;
//
//public class HttpSessionRequestCacheTest {
//
//    private MockHttpServletRequest request;
//    private MockHttpServletResponse response;
//    private HttpSessionRequestCache cache;
//    private SavedRequest savedRequest;
//    private PortResolver portResolver;
//    private MockHttpServletRequest otherRequest;
//
//    @Before
//    public void before() {
//        request = new MockHttpServletRequest();
//        response = new MockHttpServletResponse();
//        cache = new HttpSessionRequestCache();
//        portResolver = new PortResolverImpl();
//        otherRequest = new MockHttpServletRequest();
//        savedRequest = new DefaultSavedRequest(otherRequest, portResolver);
//    }
//
//    /**
//     * If no session, getRequest returns is null
//     */
//    @Test
//    @Ignore
//    public void testGetRequestNoSessionNull() {
//        Assert.assertNull(cache.getRequest(request, response));
//    }
//
//    /**
//     * If some session, but request attribute not set, getRequest returns null
//     */
//    @Test
//    @Ignore
//    public void testGetRequestSessionNoAttributeNull() {
//        request.getSession();
//        Assert.assertNull(cache.getRequest(request, response));
//    }
//
//    /**
//     * If some session, and request attribute set, returns that
//     */
//    @Test
//    @Ignore
//    public void testRemoveRequest() {
//        request.getSession().setAttribute("SPRING_SECURITY_SAVED_REQUEST", savedRequest);
//        cache.removeRequest(request, response);
//        Assert.assertNull(request.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST"));
//    }
//}
